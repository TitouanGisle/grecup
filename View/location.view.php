<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../View/CSS/location.css">
    <link rel="stylesheet" type="text/css" href="../View/CSS/body.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/header.css"/>
    <title>Grecup' Location</title>
  </head>
  <body>
    <?php include_once('header.ctrl.php'); ?>

    <!-- haut de page-->
    <h1>Nos meubles disponibles à la location</h1>

    <p>Filtrer la recherche</p>
    <form action="../Controler/location.ctrl.php" method="get">
      <label for="dateDebut">Date de début pour la location : </label>
      <input type="date" name="dateDebut" >
      <label for="dateFin">Date de fin : </label>
      <input type="date" name="dateFin">
      <label for="categorie">Catégorie : </label>
      <select name="categorie">
        <option value="tous">Tous</option>
        <option value="chill">Chill</option>
        <option value="bar">Bar</option>
        <option value="enfant">Enfant</option>
      </select>
      <input type="submit" value="Valider">
    </form>

    <?php
      if (isset($this->erreur)) {
        echo "<p>$this->erreur</p>";
      }
      else if (isset($this->dateDebut) || isset($this->categorie)) {
        echo "<p>Résultats ";
        if (isset($this->dateDebut)) {
          echo "pour la période du $this->dateDebut au $this->dateFin ";
        }
        if (isset($this->categorie)) {
          echo "pour la catégorie $this->categorie";
        }
        echo "</p>";
      }


    // On boucle sur les meubles louables qui vérifient les conditions entrées
      foreach ($this->meubles as $meuble) {
        echo "<article class=\"icone\">";
        echo  "<div id=\"miniature\">";
        echo    "<img src=\"$meuble->idMeuble\" alt=\"$meuble->idMeuble\">";
        echo  "</div>";
        echo  "<div id=\"descriptionMeubleIcone\">";
        echo    "<p>$meuble->intitule</p>";
        echo  "</div>";
        echo "</article>";
      }

    ?>

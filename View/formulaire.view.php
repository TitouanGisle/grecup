<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../View/CSS/body.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/header.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/formulaires.css"/>
    <title>Grecup' formulaire de demande</title>
  </head>
  <body>
    <?php include_once('header.ctrl.php'); ?>

    <h1>Récapitulatif de votre demande</h1>

    <p>Suite à la validation de ce formulaire, nous vous répondrons dans les
      plus brefs délais afin de convenir d'un accord.</p>

    <form method="get" action="../Controler/envoiMail.ctrl.php">
      <fieldset>
        <legend>Vos coordonnées</legend>

        <label for="nom">Nom ou raison sociale</label>
        <input type="text" name="nom" id="nom">
        <br/>

        <label for="email">adresse mail de contact</label>
        <input type="email" name="email" id="email" required>
        <br/>
      </fieldset>

      <fieldset>
        <legend>Produits demandés</legend>

        <!-- affichage des locations -->
        <h2>Location</h2>
        <?php
          // s'il y a des location on crée un tableau
          if (count($this->transactions['locations']) != 0)  {
        ?>
        <table>
          <tr>
            <th>Intitulé</th>
            <th>Categorie</th>
            <th>Quantité</th>
            <th>Du</th>
            <th>Au</th>
          </tr>
          <?php
            foreach ($this->locations as $transaction) {
              echo "<tr>";
              echo "<td>".$transaction->element->intitule."</td>";
              echo "<td>".$transaction->element->categorie."</td>";
              echo "<td>".$transaction->quantite."</td>";
              echo "<td>".$transaction->dateDebut."</td>";
              echo "<td>".$transaction->dateFin."</td>";
              echo "</tr>";
            }
           ?>
        </table>
        <?php } // fin du if ?>

        <!-- affichage des ventes -->
        <h2>Achat</h2>
        <?php
          // s'il y a des location on crée un tableau
          if (count($this->transactions['ventes']) != 0)  {
        ?>
        <table>
          <tr>
            <th>Intitulé</th>
            <th>Categorie</th>
            <th>Quantité</th>
            <th>Prix à l'unité</th>
            <th>Prix total</th>
          </tr>
          <?php
            foreach ($this->transactions['ventes'] as $transaction) {
              $quantite = $transaction->quantite;
              $prixU = $transaction->element->prixVente;
              echo "<tr>";
              echo "<td>".$transaction->element->intitule."</td>";
              echo "<td>".$transaction->element->categorie."</td>";
              echo "<td>".$quantite."</td>";
              echo "<td>".$prixU."</td>";
              echo "<td>".$prixU*$quantite."</td>";
              echo "</tr>";
            }
           ?>
        </table>
        <?php } // fin du if ?>
      </fieldset>

      <fieldset>
        <legend>Souhaitez vous être livré ?</legend>
        <div class="livraison">
          <input type="radio" name="livraison" value="oui" id="oui">
          <label for="oui">Oui</label>

          <input type="radio" name="livraison" value="non" checked id="non">
          <label for="non">Non</label>
        </div>

          <label for="adresse">Adresse de livraison</label>
          <br/>
          <textarea name="adresse" id="adresse" placeholder="" rows="4"></textarea>
          <br/>
      </fieldset>

      <fieldset>
        <legend>Votre message</legend>
        <textarea id="message" name="message" placeholder="Ecrivez votre message" rows="10"></textarea>
      </fieldset>

      <div class="bouton">
        <input type="submit" value="Envoyez">
      </div>
    </form>
  </body>
</html>

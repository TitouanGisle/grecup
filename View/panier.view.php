<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../View/CSS/body.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/header.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/panier.css"/>
    <title>Grecup' Panier</title>
  </head>
  <body>
      <?php include('header.ctrl.php') ?>

    <!-- haut de page-->
    <h1>Votre panier</h1>

    <!--Blocs correspondants à des articles du panier-->
<?php
    //si on a des ventes dans le panier:
    if (count($this->transactions['ventes']) > 0) {
        echo "<h2>Ventes : </h2>";
        //on boucle dessus

        foreach ($this->transactions['ventes'] as $vente):
            //on récupère l'élément (meuble ou package)
            $element = $vente->element;
            //et on affiche un encadré avec ses détails
?>
            <article class="item">
                <div id="miniature">
                    <img src="<?= $element->photo ?>" alt="<?= $element->idMeuble ?>"
                    height="100px" width="100px"/>
                </div>
                <div id="descriptionItem">
                    <h3><?= $element->intitule ?></h3>
                    <!-- à transformer en <form>, à l'occas',
                    pour pouvoir modifier la qté-->
                    <div id="prix">
                        <p id="prixU">Prix à l'unité : <?= $element->prixVente ?> €</p>
                        <p id="qte">Quantité : <?= $vente->quantite ?></p>
                        <p id="prixT">Sous-total : <?= $element->prixVente * $vente->quantite ?></p>
                    </div>
                </div>
                <div id="delete">
                    <a href="consulterpanier.ctrl.php?suppression=<?= $element->intitule?>">[x]</a>
                </div>
            </article>
<?php
endforeach;
}
    //si on a des locations dans le panier:
    if (count($this->transactions['locations']) > 0) {
        echo "<h2>Ventes : </h2>";
        //on boucle dessus
        foreach ($this->transactions['locations'] as $location):
            //on récupère l'élément (meuble ou package)
            $element = $location->element;
            //et on affiche un encadré avec ses détails
?>
            <article class="item">
                <div id="miniature">
                    <img src="<?= $element->photo ?>" alt="<?= $element->idMeuble ?>"
                    height="100px" width="100px"/>
                </div>
                <div id="descriptionItem">
                    <h3><?= $element->intitule ?></h3>
                    <!-- à transformer en <form>, à l'occas',
                    pour pouvoir modifier la qté-->
                    <p id="qte">Quantité : <?= $location->quantite ?></p>
                    <!--Du dateDebut au dateFin-->
                </div>
                <div id="delete">
                    <a href="consulterpanier.ctrl.php?suppression=<?= $element->intitule?>">[x]</a>
                </div>
            </article>
<?php
        endforeach;
    }
?>

    <!-- Bas de page -->
        <form action="formulaire.ctrl.php" method="post">
            <input type="submit" value="Valider mon panier"/><br/>
        </form>

    </body>
</html>

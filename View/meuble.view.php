<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../View/CSS/body.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/header.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/meuble.css"/>
    <title>G'recup - <?= $this->intitule ?></title>
  </head>
  <body id="meuble">
      <?php include_once('header.ctrl.php') ?>

<div id="description">
    <div id ="apercu">
        <img src="<?=$this->image?>" alt="photo <?= $this->intitule ?>">
    </div>
    <aside>
        <h1><?= $this->intitule ?></h1>
        <p><?= $this->descriptif ?></p>
    </aside>
</div>
<div id="transaction">
    <!-- afficher ou non si loubale/achetable -->
    <div id="achat">
        <i><b>Prix à l'unité : </b><?= $this->prixVente?></i>
        <form action="ajouterAuPanier.ctrl.php">
            <input type="hidden" name="idMeuble" value="<?=$this->id?>">
            <input type="number" name="quantite" min="1" max="666" value="1">
            <input type="submit" value="Ajouter l'achat au panier">
        </form>
    </div>
    <div id="location">
        <form action="ajouterAuPanier.ctrl.php">
            <label for="dateDebut">Du </label>
            <input type="date" name="dateDebut" >
            <label for="dateFin"> au </label>
            <input type="date" name="dateFin"><br/>
            <input type="number" name="quantite" min="1" max="666" value="1">
            <input type="submit" value="Ajouter la location au panier">
            <input type="hidden" name="idMeuble" value="<?=$this->id?>">
        </form>
    </div>
</div>
</body>
</html>

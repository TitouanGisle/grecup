<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../View/CSS/body.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/header.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/formulaires.css"/>
    <title>G'recup contact</title>
  </head>
  <body>
    <?php include_once('header.ctrl.php'); ?>

    <div class="container">
      <form method="get" action="../Controler/contact.ctrl.php">
        <fieldset>
          <legend>Vos coordonnées</legend>

          <label for="nom">Nom : </label>
          <input type="text" id="nom" name="nom" placeholder="Votre nom"> <br>

          <label for="mail">Mail : </label>
          <input type="mail" id="mail" name="mail" placeholder="Votre mail">
        </fieldset>

        <fieldset>
          <legend>Votre demande</legend>

          <label for="objet">Objet du message : </label>
          <input type="text" id="objet" name="objet" placeholder="Entrez l'objet du message ..."> <br>

          <label for="saisie">Texte</label> <br>
          <textarea id="saisie" name="saisie" placeholder="Entrez le message ..." style="height:200px"></textarea>
        </fieldset>

        <input type="submit" value="Envoyer mail">
      </form>
    </div>
  </body>
</html>

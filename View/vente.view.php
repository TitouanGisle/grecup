<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../View/CSS/vente.css">
    <link rel="stylesheet" type="text/css" href="../View/CSS/body.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/header.css"/>
    <title>Grecup' Vente</title>
  </head>
  <body>
    <?php include('header.ctrl.php'); ?>

    <!-- haut de page-->
    <h1>Nos meubles disponibles à l'achat</h1>


<?php
    // Boucle sur chaque catégorie
    foreach ($this->vendables as $categorie=>$meubles):

    echo "<h2>".$categorie."</h2>";
        //boucle sur chaque meuble de cette catégorie
        foreach ($meubles as $meuble):
?>

        <article class="meubleGalerie">
            <div id="photo">
                <a href="meuble.ctrl.php?idMeuble=<?=$meuble->idMeuble?>">
                    <img src="<?=$meuble->idMeuble?>" alt="<?=$meuble->idMeuble?>">
                </a>
            </div>
            <div id="descriptionMeubleGalerie">
                <a href="meuble.ctrl.php?idMeuble=<?=$meuble->idMeuble?>">
                    <h3><?=$meuble->intitule?></h3>
                </a>
                <p id="description">
                    <?=$meuble->descriptif?>
                </p>
                <p id="prix"><?=$meuble->prixVente?> €</p>
            </div>
        </article>
<?php
        endforeach;
    endforeach;
?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Grecup'</title>
    <link rel="stylesheet" type="text/css" href="../View/CSS/body.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/header.css"/>
    <link rel="stylesheet" type="text/css" href=""/>
    <title>G'recup demande envoyée</title>
  </head>
  <body>
    <?php include_once('header.ctrl.php'); ?>

    <?php
      // affiche un message de confirmation ou d'erreur
      if ($this->envoi) {
        echo "<p> un message a été envoyé à l'expediteur, il vous répondra dès que possible </p>";
      }
      else {
        echo "<p> échec lors de l'envoi du mail, veuillez réessayer </p>";
      }
     ?>

  </body>
</html>

<?php
// Controleur formulaire : appelé lors de la validation du panier
// Objectif : pré-remplir un formulaire
require_once('../Model/Element.class.php');
require_once('../Model/Transaction.class.php');
require_once('../Model/Panier.class.php');
require_once('../Model/View.class.php');
require_once('../Model/Panier.class.php');
require_once('../Model/Element.class.php');
require_once('../Model/Transaction.class.php');

//////////////////////////////////////////////////////////////////////////////
// PARTIE RECUPERATION DES DONNEES
//////////////////////////////////////////////////////////////////////////////
// toutes les données nécessaires sont dans $_SESSION
session_start();
$panier = $_SESSION['panier'];

//////////////////////////////////////////////////////////////////////////////
// PARTIE SELECTION DE LA VUE
//////////////////////////////////////////////////////////////////////////////
$view = new View('../View/formulaire.view.php');
$view->transactions = $panier->getTransactions();
$view->show();

?>

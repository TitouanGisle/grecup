<?php
// Controleur location : celui qui est concerné lorsque le visiteur clique sur l'onglet "Location"
// Objectif : générer la vue qui affiche tous les meubles disponibles à la location

require_once('../Model/Element.class.php');
require_once('../Model/Transaction.class.php');
require_once('../Model/Panier.class.php');
require_once('../Model/DAOLocation.class.php');
require_once('../Model/View.class.php');
session_start();
//////////////////////////////////////////////////////////////////////////////
// PARTIE RECUPERATION DES DONNEES
//////////////////////////////////////////////////////////////////////////////

// Récupération des informations de la query string

// Les dates de début et de fin de la location éventuellement entrées par le visiteur
if (isset($_GET['dateDebut']) && $_GET['dateDebut']!='') {
  $dateDebut = date('d/m/Y', strtotime($_GET['dateDebut']));
  if (isset($_GET['dateFin']) && $_GET['dateFin']!='') {
    $dateFin = date('d/m/Y', strtotime($_GET['dateFin']));
  }
  else {
    $erreur = 'Veuillez entrer une date de fin.';
  }
}

// La catégorie éventuellement entrée par le visiteur
if (isset($_GET['categorie']) && $_GET['categorie']!='' && $_GET['categorie']!='tous') {
  $categorie = $_GET['categorie'];
}

/////////////////////////////////////////////////////////////////////////////
// PARTIE USAGE DU MODELE
//////////////////////////////////////////////////////////////////////////////

$location = new DAOLocation;
if (isset($categorie) && !isset($dateDebut)) {
  $parametres = array("categorie" => $categorie);
  $louables = $location->getLouables($parametres);
}
else if (!isset($categorie) && isset($dateDebut) && isset($dateFin)) {
  $parametres = array("dateDebut" => $dateDebut, "dateFin" => $dateFin);
  var_dump($parametres);
  $louables = $location->getLouables($dateDebut, $dateFin);
}
else if (isset($categorie) && isset($dateDebut) && isset($dateFin)) {
  $parametres = array("dateDebut" => $dateDebut, "dateFin" => $dateFin, "categorie" => $categorie);
    var_dump($parametres);
  $louables = $location->getLouables($categorie, $dateDebut, $dateFin);
}
else {
  $louables = $location->getLouables($parametres);
}

//////////////////////////////////////////////////////////////////////////////
// PARTIE GENERATION DE LA VUE
//////////////////////////////////////////////////////////////////////////////

$view = new View('../View/location.view.php');

if (isset($erreur)) { // Si le visiteur a entré une date de début mais pas de date de fin
  $view->erreur = $erreur;
}
else if (isset($dateDebut)) { // Cas où les deux dates ont été entrées
  $view->dateDebut = $dateDebut;
  $view->dateFin = $dateFin;
}

if (isset($categorie)) {
  $view->categorie = $categorie;
}

$view->meubles = $louables;
$view->show();

?>

<?php
// Controleur start : appelé lors de l'arrivée sur le site
// Objectif : générer la page d'accueil

require_once('../Model/Element.class.php');
require_once('../Model/Transaction.class.php');
require_once('../Model/Panier.class.php');
require_once('../Model/View.class.php');
session_start();
//////////////////////////////////////////////////////////////////////////////
// PARTIE RECUPERATION DES DONNEES
//////////////////////////////////////////////////////////////////////////////
// rien à récupérer mais création d'un panier s'il n'existe pas encore
if (!isset($_SESSION['panier'])) {
  $_SESSION['panier'] = new Panier();
}

//////////////////////////////////////////////////////////////////////////////
// PARTIE SELECTION DE LA VUE
//////////////////////////////////////////////////////////////////////////////
$view = new View('../View/homepage.view.php');
$view->show();

?>

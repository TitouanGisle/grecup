<?php
// Contoleur AjouterAuPanier : Il est appele quand on appuie sur le bouton Acheter ou Louer

require_once('../Model/MeubleDAO.class.php');
require_once('../Model/Element.class.php');
require_once('../Model/Transaction.class.php');
require_once('../Model/Panier.class.php');
require_once('../Model/View.class.php');
session_start();

// Recupere les donnes du GET
// FAIRE DES TESTS D ERREUR
$idMeuble = htmlentities($_GET["idMeuble"]);
$quantite = (int) htmlentities($_GET["quantite"]);

/////////////////////////////////////////////////////////////////////////////
// PARTIE USAGE DU MODELE
//////////////////////////////////////////////////////////////////////////////

// Recupere le Meuble avec la DAO
$DAO = new MeubleDAO();
$element = $DAO->getMeuble($idMeuble);

// Recupere le panier
//$panier = $_SESSION['panier'];
$panier = (isset($_SESSION['panier'])) ? $_SESSION['panier'] : new Panier();
$found = false;

// Cree la transaction
// Si dateDebut existe, alors la transaction doit etre une location
if (!isset($_GET["dateDebut"])) {
	$transaction = new Transaction($element, $quantite);
} else {
	$dateDebut = htmlentities($_GET["dateDebut"]);
	$dateFin = htmlentities($_GET["dateFin"]);
	$transaction = new Location($element, $quantite, $dateDebut, $dateFin);
}

// si une transaction pour le me produit existe déjà on rajoute à la quantité
foreach ($panier->transactions as $t) {
	if ($t->memeTransaction($transaction)) {
		$t->quantite+=$transaction->quantite;
		$found = true;
		break;
	}
}

if (!$found) $panier->ajouterTransaction($transaction);

//on enregistre le panier dans les variables de session
$_SESSION['panier'] = $panier;

// Renvoie la page du meuble
header('Location: meuble.ctrl.php?idMeuble='.$idMeuble);

?>

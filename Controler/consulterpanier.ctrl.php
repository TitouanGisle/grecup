<?php
// Controleur consulterPanier : celui qui est concerné lorsque le visiteur clique sur l'onglet "Panier"
// Objectif : générer la page qui affiche le contenu du panier du client

require_once('../Model/Element.class.php');
require_once('../Model/Transaction.class.php');
require_once('../Model/Panier.class.php');
require_once('../Model/View.class.php');
session_start();
//////////////////////////////////////////////////////////////////////////////
// PARTIE RECUPERATION DES DONNEES
//////////////////////////////////////////////////////////////////////////////
//récupération des éléments du panier
if (isset($_SESSION['panier'])) {
    $panier = $_SESSION['panier'];
//ou création si on arrive là sans que le panier n'aie été créé, pour une quelconque raison
} else {
    $_SESSION['panier'] = new Panier();
    $panier = $_SESSION['panier'];
}

//////////////////////////////////////////////////////////////////////////////
// PARTIE USAGE DU MODELE
//////////////////////////////////////////////////////////////////////////////

//cas où l'on arrive sur cette page depuis une suppression
if (isset($_GET['suppression'])) {
    $aSupprimer = $_GET['suppression'];

    //on cherche l'id de la transaction à supprimer
    $t = $aSupprimer->getIdTransation();
    //on enlève la transaction à cette id
    unset($panier->transactions[$t]);
    //puis on 'regroupe' l'array
    $panier->transactions = array_values($panier->transactions);
    //et on met à jour le Panier enregistré
    $_SESSION['panier'] = $panier;
}

//séparation des transactions entre les locations et les ventes
$locations = array(); $ventes = array();
foreach ($panier->transactions as $transaction) {
    if(isset($transaction->dateDebut)){
        array_push($locations, $transaction);
    } else {
        array_push($ventes, $transaction);
    }
}


//////////////////////////////////////////////////////////////////////////////
// PARTIE SELECTION DE LA VUE
//////////////////////////////////////////////////////////////////////////////
$view = new View('panier.view.php');
$view->transactions = $panier->getTransactions();
$view->show();
?>

<?php
// Controleur envoiMail : appelé lors de la validation du formulaire de demande
// Objectif : confirme l'envoi ou non du mail
// s'il a bien été envoyé vide le panier

require_once('../Model/Element.class.php');
require_once('../Model/Transaction.class.php');
require_once('../Model/Panier.class.php');
require_once('../Model/View.class.php');
require_once('../Model/Mail.class.php');

//////////////////////////////////////////////////////////////////////////////
// PARTIE RECUPERATION DES DONNEES
//////////////////////////////////////////////////////////////////////////////
// récupération des donées du formulaire
$expediteur = htmlentities($_GET["nom"]);
$email = htmlentities($_GET["email"]);
$livraison = htmlentities($_GET["livraison"]);
$adresse = htmlentities($_GET["adresse"]);
$message = htmlentities($_GET["message"]);

// récupération des produits du panier
session_start();
$panier = $_SESSION["panier"];

// génération et envoi du mail
$mail = new Mail($email, $expediteur, $message);
$envoi = $mail->ecrireMailDemande($panier, $livraison, $adresse);

// si le mail a été envoyé réinitialisation du panier
if ($envoi) {
  $_SESSION['panier'] = new Panier();
}

//////////////////////////////////////////////////////////////////////////////
// PARTIE SELECTION DE LA VUE
//////////////////////////////////////////////////////////////////////////////
// Retour à la page d'accueil
$view = new View('../View/confirmation.view.php');
$view->envoi = $envoi;
$view->show();

?>

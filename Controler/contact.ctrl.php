<?php
require_once('../Model/Panier.class.php');
require_once('../Model/Transaction.class.php');
require_once('../Model/View.class.php');
require_once('../Model/Mail.class.php');

//////////////////////////////////////////////////////////////////////////////
// PARTIE RECUPERATION DES DONNEES
//////////////////////////////////////////////////////////////////////////////
session_start();
$envoi = FALSE;
$ope = 0;

// si pas de paramètre on affiche le formulaire, sinon on traite le formulaire
if (!isset($_GET['mail'])) {
  $ope = 1;
}
else { // message ok
  // on récupère toute les infos du formulaire
  $nom = htmlentities($_GET['nom']);
  $mail = htmlentities($_GET['mail']);
  $objet = htmlentities($_GET['objet']);
  $text = htmlentities($_GET['saisie']);

  // génération et envoi du mail
  $mail = new Mail($mail, $nom, $text);
  $envoi = $mail->ecrireMailContact($objet);
}

//////////////////////////////////////////////////////////////////////////////
// PARTIE SELECTION DE LA VUE
//////////////////////////////////////////////////////////////////////////////
if ($ope) {
  $view = new View('../View/contact.view.php');
  $view->show();
}
else {
  $view = new View('../View/confirmation.view.php');
  $view->envoi = $envoi;
  $view->show();
}

 ?>

<?php
//Génération de l'affichage d'un meuble
//dont l'id est passée via $_GET

require_once('../Model/MeubleDAO.class.php');
require_once('../Model/Element.class.php');
require_once('../Model/Transaction.class.php');
require_once('../Model/Panier.class.php');
require_once('../Model/View.class.php');
session_start();

$ini = parse_ini_file('../Config/config.ini');
$idMeuble = htmlentities($_GET["idMeuble"]);
$DAO = new MeubleDAO();

$meuble = $DAO->getMeuble($idMeuble);


  // $meuble = $DAO->getMeuble($idMeuble);
  //------------------------------
  $view = new View('meuble.view.php');
  // $view->meuble = $meuble;
  $view->id=$idMeuble;
  $view->intitule = $meuble->intitule;
  $view->categorie = $meuble->categorie;
  $view->vendable = $meuble->vendable;
  $view->louable = $meuble->louable;
  $view->descriptif = $meuble->descriptif;
  $view->prixVente = $meuble->prixVente;
  $view->image = $meuble->photo;
  $view->show();
?>

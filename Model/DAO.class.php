<?php

class DAO {
	// classe Mere DAO

	protected $db;

	function __construct() {
		$database = 'sqlite:'.__DIR__.'/Data/base.db';
		try {
			$this->db = new PDO($database);
		}
		catch (PDOException $e) {
			die("Erreur de connexion : ".$e->getMessage()."\n");
		}
	}
}
?>

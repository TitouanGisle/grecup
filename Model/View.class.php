<?php

// Classe minimaliste pour normaliser l'usage d'une vue
class View {
  public $path=""; // Chemin vers le fichier de la vue

  // Constructeur d'une vue
  function __construct(string $path="") {
    $this->path = $path;
  }
  // Pour rendre plus joli le lancement de la vue
  function show(string $path="") {
    // prend l'attribut si le paramètre est vide
    $p = $path ? $path : $this->path;
    // Regarde si le chemin est relatif
    if ($p[0] != '.') {
      // Ajoute le chemin relatif
      $p = "../View/".$p;
    }
    // Comme l'inclusion est dans le contexte de l'instance
    // il faut utiliser $this pour acceder aux attributs
    // Toute autre variable est invisible
    require_once($p);
  }
}
?>

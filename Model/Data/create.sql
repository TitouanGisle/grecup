DROP TABLE IF EXISTS meuble;
DROP TABLE IF EXISTS package;
DROP TABLE IF EXISTS contenu;
DROP TABLE IF EXISTS transactions;
DROP TABLE IF EXISTS image;
DROP TABLE IF EXISTS creation;
DROP TABLE IF EXISTS imageCreation;

CREATE TABLE meuble (
idMeuble TEXT PRIMARY KEY,
intitule TEXT,
categorie TEXT,
vendable INTEGER,
louable INTEGER,
descriptif TEXT,
prixVente REAL,
quantite INTEGER, -- revoir car calc
miniature TEXT
);

CREATE TABLE creation (
  idCreation TEXT PRIMARY KEY,
  intitule TEXT,
  descriptif TEXT
);

CREATE TABLE imageCreation (
  nomFichierImageCreation TEXT PRIMARY KEY,
  idCreation INTEGER references creation(idCreation)
);

CREATE TABLE image (
  nomFichierImage TEXT PRIMARY KEY,
  idMeuble INTEGER references meuble(idMeuble)
);

CREATE TABLE package (
  idPack INTEGER PRIMARY KEY,
  prix INTEGER,
  durée INTEGER
);

CREATE TABLE contenu (
  idPack INTEGER references package(idPack),
  idMeuble TEXT references meuble(idMeuble),
  quantite INTEGER
);

CREATE TABLE transactions (
  idTransaction INTEGER PRIMARY KEY,
  idMeuble TEXT references meuble(idMeuble),
  dateDebut DATE,
  dateFin DATE,
  quantite INTEGER
);

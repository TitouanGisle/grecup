------------------------------------------------
------------------------------------------------
--  INSERT des meubles
------------------------------------------------
------------------------------------------------
INSERT INTO meuble
(idMeuble,categorie,intitule,vendable,louable,descriptif,prixVente,quantite,miniature)
VALUES
("BanqSimp",
  "chill",
  "Banquette simple",
  1,
  1,
  "Lorem ipsum dolor sit amet",
  90.00,
  8,
  "BanqSimp.PNG");
------------------------------------------------
INSERT INTO meuble
(idMeuble,categorie,intitule,vendable,louable,descriptif,prixVente,quantite,miniature)
VALUES
("BanqAngOuv",
  "chill",
  "Banquette angle ouvert",
  1,
  1,
  "Lorem ipsum dolor sit amet",
  110.00,
  7,
  "BanqAngOuv.jpg");
------------------------------------------------
INSERT INTO meuble
(idMeuble,categorie,intitule,vendable,louable,descriptif,prixVente,quantite,miniature)
VALUES
("BanqAngFerm",
  "chill",
  "Banquette angle fermé",
  1,
  1,
  "Lorem ipsum dolor sit amet",
   110.00,
   8,
    "BanqAngFerm.jpg");
------------------------------------------------
INSERT INTO meuble
(idMeuble,categorie,intitule,vendable,louable,descriptif,prixVente,quantite,miniature)
VALUES
("BanqRom",
  "chill",
  "Banquette romaine",
  1,
  1,
  "Lorem ipsum dolor sit amet",
   335.00,
   2,
    "BanqRom.PNG");
------------------------------------------------
INSERT INTO meuble
(idMeuble,categorie,intitule,vendable,louable,descriptif,prixVente,quantite,miniature)
VALUES
("BancRect",
  "chill",
  "Banc rectangulaire",
  1,
  1,
  "Lorem ipsum dolor sit amet",
   175.00,
    8,
  "BancRect.PNG");
------------------------------------------------
INSERT INTO meuble
(idMeuble,categorie,intitule,vendable,louable,descriptif,prixVente,quantite,miniature)
VALUES
("BancArrond",
  "chill",
  "Banc arrondi",
  1,
  1,
  "Lorem ipsum dolor sit amet",
   125.00,
    7,
    "BancArrond.PNG"
);
------------------------------------------------
INSERT INTO meuble
(idMeuble,categorie,intitule,vendable,louable,descriptif,prixVente,quantite,miniature)
VALUES
("ChaiHaut",
  "bar",
  "Chaise haute",
  1,
  1,
  "Lorem ipsum dolor sit amet",
   80.00,
    19,
    "ChaiHaut.jpg"
  );
------------------------------------------------
INSERT INTO meuble
(idMeuble,categorie,intitule,vendable,louable,descriptif,prixVente,quantite,miniature)
VALUES
("TabHaut",
  "bar",
  "Table haute",
  1,
  1,
  "Lorem ipsum dolor sit amet",
   155.00,
    5,
    "TabHaut.jpg"
  );
  ------------------------------------------------
  ------------------------------------------------
  --  INSERT images
  ------------------------------------------------
  ------------------------------------------------
  INSERT INTO image (idMeuble, nomFichierImage) VALUES ("TabHaut","TabHaut.jpg");
  INSERT INTO image (idMeuble, nomFichierImage) VALUES ("TabHaut","TabHaut1.jpg");
  ------------------------------------------------
  INSERT INTO image (idMeuble, nomFichierImage) VALUES ("ChaiHaut","ChaiHaut.jpg");
  INSERT INTO image (idMeuble, nomFichierImage) VALUES ("ChaiHaut","ChaiHaut2.jpg");
  ------------------------------------------------
  INSERT INTO image (idMeuble, nomFichierImage) VALUES ("BancArrond","BancArrond.PNG");
  ------------------------------------------------
  INSERT INTO image (idMeuble, nomFichierImage) VALUES ("BancRect","BancRect.PNG");
  INSERT INTO image (idMeuble, nomFichierImage) VALUES ("BancRect","BancRect1.jpg");
  ------------------------------------------------
  INSERT INTO image (idMeuble, nomFichierImage) VALUES ("BanqAngFerm","BanqAngFerm.jpg");
  INSERT INTO image (idMeuble, nomFichierImage) VALUES ("BanqAngFerm","BanqAngFerm1.jpg");
  ------------------------------------------------
  INSERT INTO image (idMeuble, nomFichierImage) VALUES ("BanqAngOuv","BanqAngOuv.jpg");
  INSERT INTO image (idMeuble, nomFichierImage) VALUES ("BanqAngOuv","BanqAngOuv1.jpg");
  ------------------------------------------------
  INSERT INTO image (idMeuble, nomFichierImage) VALUES ("BanqSimp","BanqSimp.PNG");
  INSERT INTO image (idMeuble, nomFichierImage) VALUES ("BanqSimp","BanqSimp1.jpg");
  ------------------------------------------------
  INSERT INTO image (idMeuble, nomFichierImage) VALUES ("BanqRom","BanqRom.PNG");
  INSERT INTO image (idMeuble, nomFichierImage) VALUES ("BanqRom","BanqRom1.jpg");
  ------------------------------------------------
  ------------------------------------------------
  --  INSERT creation
  ------------------------------------------------
  ------------------------------------------------
  INSERT INTO creation
  (idCreation,intitule,descriptif)
  VALUES
  ("Banc",
    "Banc et table",
    "Lorem ipsum dolor sit amet"
);
------------------------------------------------
INSERT INTO creation
(idCreation,intitule,descriptif)
VALUES
("BancBis",
  "Banc",
  "Lorem ipsum dolor sit amet"
);
------------------------------------------------
INSERT INTO creation
(idCreation,intitule,descriptif)
VALUES
("Cagette",
  "Cagette en palette",
  "Lorem ipsum dolor sit amet"
);
------------------------------------------------
INSERT INTO creation
(idCreation,intitule,descriptif)
VALUES
("ChaiseHaute",
  "Chaise hautes et mange debout",
  "Lorem ipsum dolor sit amet"
);
------------------------------------------------
INSERT INTO creation
(idCreation,intitule,descriptif)
VALUES
("ChaisePliante",
  "Chaise pliante rénovée",
  "Lorem ipsum dolor sit amet"
);
------------------------------------------------
INSERT INTO creation
(idCreation,intitule,descriptif)
VALUES
("Coffre",
  "Coffre « pirate » table de nuit, mini tabouret et plateau de service",
  "Lorem ipsum dolor sit amet"
);
------------------------------------------------
INSERT INTO creation
(idCreation,intitule,descriptif)
VALUES
("Des",
  "Dés à jouer",
  "Lorem ipsum dolor sit amet"
);
------------------------------------------------
INSERT INTO creation
(idCreation,intitule,descriptif)
VALUES
("Fauteuil",
  "Fauteuil pneu",
  "Lorem ipsum dolor sit amet"
);
------------------------------------------------
INSERT INTO creation
(idCreation,intitule,descriptif)
VALUES
("Horloge",
  "Horloge",
  "Lorem ipsum dolor sit amet"
);
------------------------------------------------
INSERT INTO creation
(idCreation,intitule,descriptif)
VALUES
("Palette",
  "Mini palette",
  "Lorem ipsum dolor sit amet"
);
------------------------------------------------
INSERT INTO creation
(idCreation,intitule,descriptif)
VALUES
("PlateauDes",
  "Plateau de dés",
  "Lorem ipsum dolor sit amet"
);
------------------------------------------------
INSERT INTO creation
(idCreation,intitule,descriptif)
VALUES
("Table",
  "Table bistrot pliable",
  "Lorem ipsum dolor sit amet"
);
------------------------------------------------
------------------------------------------------
--  INSERT image des créations
------------------------------------------------
------------------------------------------------
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Banc","Banc.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Banc","Banc1.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Banc","Banc2.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Banc","Banc3.jpg");
------------------------------------------------
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("BancBis","BancBis.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("BancBis","BancBis1.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("BancBis","BancBis2.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("BancBis","BancBis3.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("BancBis","BancBis4.jpg");
------------------------------------------------
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Cagette","Cagette.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Cagette","Cagette1.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Cagette","Cagette2.jpg");
------------------------------------------------
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("ChaiseHaute","ChaiseHaute.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("ChaiseHaute","ChaiseHaute1.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("ChaiseHaute","ChaiseHaute2.jpg");
------------------------------------------------
------------------------------------------------
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("ChaisePliante","ChaisePliante.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("ChaisePliante","ChaisePliante1.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("ChaisePliante","ChaisePliante2.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("ChaisePliante","ChaisePliante3.jpg");
------------------------------------------------
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Coffre","Coffre.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Coffre","Coffre1.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Coffre","Coffre2.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Coffre","Coffre3.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Coffre","Coffre4.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Coffre","Coffre5.jpg");
------------------------------------------------
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Des","Des.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Des","Des1.jpg");
------------------------------------------------
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Fauteuil","Fauteuil.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Fauteuil","Fauteuil2.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Fauteuil","Fauteuil3.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Fauteuil","Fauteuil4.jpg");
------------------------------------------------
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Horloge","Horloge.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Horloge","Horloge2.jpg");
------------------------------------------------
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Palette","Palette.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Palette","Palette2.jpg");
------------------------------------------------
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("PlateauDes","PlateauDes.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("PlateauDes","PlateauDes1.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("PlateauDes","PlateauDes2.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("PlateauDes","PlateauDes3.jpg");
------------------------------------------------
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Table","Table.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Table","Table1.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Table","Table2.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Table","Table3.jpg");

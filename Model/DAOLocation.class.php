<?php
require_once('Element.class.php');
require_once('DAO.class.php');

class DAOLocation extends DAO {

    // Renvoie tous les meubles louables
    function getLouables($parametres) {
      try {
        if (array_key_exists("dateDebut", $parametres) && array_key_exists("dateFin", $parametres) && array_key_exists("categorie", $parametres)) {
          $categorie = $parametres["categorie"];
          $dateDebut = $parametres["dateDebut"];
          $dateFin = $parametres["dateFin"];
          $sth = ($this->db)->query("SELECT m.idMeuble,m.intitule,m.categorie,m.louable,m.descriptif,COALESCE(m.quantite-sum(t.quantite),m.quantite) FROM meuble m
          LEFT JOIN transactions t ON m.idMeuble = t.idMeuble AND t.dateDebut > '$dateDebut' AND t.dateFin < '$dateFin'
          WHERE m.louable=1 AND categorie='$categorie' GROUP BY m.idMeuble");
          $sth->setFetchMode(PDO::FETCH_GROUP|PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Meuble');
          $louables = $sth->fetchAll();
        }
        else if (array_key_exists("dateDebut", $parametres) && array_key_exists("dateFin", $parametres)) {
          $dateDebut = $parametres["dateDebut"];
          $dateFin = $parametres["dateFin"];
          $sth = ($this->db)->query("SELECT m.idMeuble,m.intitule,m.categorie,m.louable,m.descriptif,COALESCE(m.quantite-sum(t.quantite),m.quantite) FROM meuble m
          LEFT JOIN transactions t ON m.idMeuble = t.idMeuble AND t.dateDebut > '$dateDebut' AND t.dateFin < '$dateFin'
          WHERE m.louable=1 GROUP BY m.idMeuble");
          $sth->setFetchMode(PDO::FETCH_GROUP|PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Meuble');
          $louables = $sth->fetchAll();
        }
        else if (array_key_exists("categorie", $parametres)) {
          $categorie = $parametres["categorie"];
          $sth = ($this->db)->query("SELECT * FROM meuble WHERE louable=1 AND categorie='$categorie' AND quantite>0");
          $sth->setFetchMode(PDO::FETCH_GROUP|PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Meuble');
          $louables = $sth->fetchAll();
        }
        else {
          $sth = ($this->db)->query("SELECT * FROM meuble WHERE louable=1 AND quantite>0");
          $sth->setFetchMode(PDO::FETCH_GROUP|PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Meuble');
          $louables = $sth->fetchAll();
        }

        return $louables;
      }
      catch (PDOException $e) {
        die("Erreur : ".$e->getMessage()."\n");
      }
    }
}

?>

<?php
class Mail {
	public $to; 	// si plusieurs adresses mettre un "," à la fin de chaque
  public $from; // adresse de l'expéditeur passé via le formulaire
	public $nomE; // nom de l'expéditeur passé via le formulaire
  public $subject;
  public $content; // contenu du mail
	public $messageE; // message passé via le formulaire
	public $entete;

	function __construct($from, $nom, $message, $to = "camille-billouard@outlook.fr") {
		$this->to = $to;
		// verifie que l'adresse de contact entrée est correcte
		// limite le spam et les erreurs de saisie
		if ($this->isSyntaxeOk($from)) $this->from = $from;
		else $this->from = NULL;
		$this->nomE = $nom;
		$this->subject = "Envoi depuis G'recup par $nom";
		$this->content = $message;
		$this->messageE = $message;
		// création de l'entête
		$entete = "Reply-To:".$this->from."\r\n";
	}

	function ecrireMailDemande($panier, $livraison, $adresse) {
		$ok = FALSE;
		if ($this->isSyntaxeOk($this->to) && $this->isSyntaxeOk($this->from)) {
			// Création des chaînes de caractère à passer à la fonction mail()
			$this->subject = "Demande de ".$this->nomE;
			$transactions = $panier->getTransactions();

	    // création de la liste des demandes
			// séparation des ventes et des locations
	    // choix : mode text et non html pour assurer la compatibilité avec tous les opérateurs
	    // pour mettre de la mise en forme il faut passer en mode html
			// \r\n retour à la ligne standard pour mail mais parfois mal pris en compte par les boites mail
			// si problèmes d'affichage mettre simplement \n
	    $listeLocations = "";
	    foreach ($transactions['locations'] as $transaction) {
	      $listeLocations .= "\r\n"
	        .$transaction->quantite." ".$transaction->element->intitule
	        ." de la catégorie ".$transaction->element->categorie."\r\n"
	        ."Du".$transaction->dateDebut." au ".$transaction->dateFin."\r\n";
	    }

	    // création de la chaine correspondant aux ventes
	    $listeVentes = "";
	    foreach ($transactions['ventes'] as $transaction) {
	        $quantite = $transaction->quantite;
	        $prixU = $transaction->element->prixVente;

	        $listeVentes .= "\r\n"
	          .$quantite." ".$transaction->element->intitule
	          ." de la catégorie ".$transaction->element->categorie."\r\n"
	          ."Prix à l'unité : ".$prixU."\r\n"
	          ."Prix total : ".$prixU*$quantite."\r\n";
	    }

	    $this->content = "Nom de l'emetteur : ".$this->nomE."\r\n"
	      ."Adresse mail de contact : ".$this->from."\r\n"
	      ."\r\n"."Message de l'emetteur : "."\r\n".$this->messageE."\r\n"
	      ."\r\n"."Produits à louer : \r\n".$listeLocations."\r\n"
	      ."\r\n"."Produits à vendre : \r\n".$listeVentes."\r\n"
	      ."\r\n"."livraison : ".$livraison."\r\n";
	    if (isset($adresse)) {
	      $this->content .= $adresse;
	    }

    	$ok = $this->envoyerMail();
		}
		return $ok;
	}

  function ecrireMailContact($objet) {
		$ok = FALSE; // code de retour
		if (($this->from != '') && ($objet != '') && ($this->messageE != '') && $this->isSyntaxeOk($this->to) && $this->isSyntaxeOk($this->from)) {
			$this->subjet = $objet;

	    $entete = 'From:'.$this->nomE.' <'.$this->from.'>' . "\r\n" .
	  				'Reply-To:'.$this->from. "\r\n" .
	  				'Content-Type: text/plain; charset="utf-8"; DelSp="Yes"; format=flowed '."\r\n" .
	  				'Content-Disposition: inline'. "\r\n" .
	  				'Content-Transfer-Encoding: 7bit'." \r\n" .
	  				'X-Mailer:PHP/'.phpversion();

    	$ok = $this->envoyerMail();
		}
		return $ok;
	}

	function isSyntaxeOk($email) {
		// pour "preg_match" voir la doc : https://www.php.net/manual/fr/function.preg-match.php)
		$val = preg_match('/^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9_](?:[a-zA-Z0-9_\-](?!\.)){0,61}[a-zA-Z0-9_-]?\.)+[a-zA-Z0-9_](?:[a-zA-Z0-9_\-](?!$)){0,61}[a-zA-Z0-9_]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/', $email);
		return (($val === 0) || ($val === false)) ? false : true;
	}

  private function envoyerMail() {
    return mail($this->to, $this->subject, $this->content, $this->entete);
	}
}

?>

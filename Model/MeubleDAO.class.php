<?php

require_once('DAO.class.php');
require_once('Element.class.php');

class MeubleDAO extends DAO {

	function getMeuble(string $id) {
		try {
			$query = $this->db->query("SELECT * FROM meuble WHERE idMeuble = '$id'");
			$result = $query->fetchAll(PDO::FETCH_CLASS, "Meuble");
		} catch (PDOException $e) {
			die("PDO Error :".$e->getMessage());
		}

		if (sizeof($result) == 0) {
			die("No value found for '$id'\n");
		}

		return $result[0];
	}
}

?>

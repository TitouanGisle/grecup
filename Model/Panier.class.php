<?php
class Panier {
	public $transactions; // Tableau de Transaction

	function __construct() {
		$this->transactions = array();
	}

	function ajouterTransaction(Transaction $transaction) {
		array_push($this->transactions, $transaction);
	}

	function getTransactions()
	// renvoie un tableau contenant un tableau de transactions de type location et un de celles de types vente
	{
		$ventes = array();
		$locations = array();
		$transactions = array();

		foreach ($this->transactions as $transaction) {
			if (isset($transaction->dateDebut)) array_push($locations, $transaction);
			else array_push($ventes, $transaction);
		}

		$transactions['locations'] = $locations;
		$transactions['ventes'] = $ventes;
		return $transactions;
	}

	function getPrixTotalVentes() {}

	//renvoie la quantite d'Elements contenus dans le panier
	function getQuantitePanier() {
		$qte = 0;
		foreach($this->transactions as $transaction) {
			$qte+=$transaction->quantite;
		}
		return $qte;
	}
}

?>

<?php
require_once('Element.class.php');
require_once('DAO.class.php');

class DAOVente extends DAO {

    // private $db;
    //
    // // Constructeur
    // function __construct() {
    //   $config = parse_ini_file('../Config/config.ini');
    //   $database = 'sqlite:'.$config['database_path'].'/base.db';
    //   try {
    //     $this->db = new PDO($database);
    //   }
    //   catch (PDOException $e) {
    //     die("Erreur de connexion : ".$e->getMessage()."\n");
    //   }
    // }

    // Renvoie tous les meubles vendables chill
    function getVendablesChill() {
      try {
        $sth = ($this->db)->query("SELECT * FROM meuble WHERE vendable=1 AND categorie='chill' AND quantite>0");
        $sth->setFetchMode(PDO::FETCH_GROUP|PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Meuble');
        $vendables = $sth->fetchAll();
        return $vendables;
      }
      catch (PDOException $e) {
        die("Erreur : ".$e->getMessage()."\n");
      }
    }

    // Renvoie tous les meubles vendables bar
    function getVendablesBar() {
      try {
        $sth = ($this->db)->query("SELECT * FROM meuble WHERE vendable=1 AND categorie='bar' AND quantite>0");
        $sth->setFetchMode(PDO::FETCH_GROUP|PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Meuble');
        $vendables = $sth->fetchAll();
        return $vendables;
      }
      catch (PDOException $e) {
        die("Erreur : ".$e->getMessage()."\n");
      }
    }

    // Renvoie tous les meubles vendables enfant
    function getVendablesEnfant() {
      try {
        $sth = ($this->db)->query("SELECT * FROM meuble WHERE vendable=1 AND categorie='enfant' AND quantite>0");
        $sth->setFetchMode(PDO::FETCH_GROUP|PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Meuble');
        $vendables = $sth->fetchAll();
        return $vendables;
      }
      catch (PDOException $e) {
        die("Erreur : ".$e->getMessage()."\n");
      }
    }

    function getVendablesByCat() {
        //renvoie un array d'arrays de Meubles
        try {
            //récupération des différentes catégories :
            $categories = ($this->db)->query('SELECT DISTINCT categorie FROM meuble WHERE vendable=1 AND quantite>0')->fetchAll(PDO::FETCH_COLUMN);

            $vendables = array();
            /*chill => banquettesimple, chaiselongue...
            bar => chaiseHaute, minibar...
            enfants => dés en bois, cheval à bascule...*/

            //récupération, pour chaque catégorie, des meubles concernés
            foreach ($categories as $key => $categorie) {
                $sth = ($this->db)->query("SELECT * FROM meuble WHERE categorie='$categorie' AND vendable=1 AND quantite>0");
                $sth->setFetchMode(PDO::FETCH_GROUP|PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Meuble');
                $meubles = $sth->fetchAll();
                $vendables["$categorie"]=$meubles;
            }
            return $vendables;
        }
        catch (PDOException $e) {
            die("Erreur : ".$e->getMessage()."\n");
        }
    }
}

?>

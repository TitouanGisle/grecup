<?php

class Transaction {
	// Une transaction de base est une vente

	public $element;
	public $quantite;

	function __construct(Element $element, int $quantite) {
		$this->element = $element;
		$this->quantite = $quantite;
	}

	function memeTransaction($other) {
		if (isset($other->dateDebut)) {
			return false;
		} else {
			return $other->element == $this->element;
		}
	}

	function getIdTransaction() {
    $i = 0;
    while (isset($panier->transactions[$i]) && $panier->transactions[$i]->element->intitule!=$aSupprimer){
        $i++;
    }
		return $i;
	}
}

class Location extends Transaction {
	// La date fin est fixe si l'element est un package

	public $dateDebut; // DateTime
	public $dateFin; // DateTime
	public $validee; // boolean	La transaction a-t-elle ete validee par l'equipe (doit elle etre prise en compte pour les autres clients)

	function __construct(Element $element, int $quantite, DateTime $dateDebut, DateTime $dateFin) {
		$this->element = $element;
		$this->quantite = $quantite;
		$this->dateDebut = $dateDebut;
		$this->dateFin = $dateFin;
	}

	function memeTransaction($other) {
		if (!isset($other->dateDebut)) {
			return false;
		} else {
			return $other->element == $this->element
				&& $other->dateDebut == $this->dateDebut
				&& $other->dateFin == $this->dateFin;
		}
	}

	function setDateDebut($dateDebut) {}
	function getDateDebut() {}
	function setDateFin($dateFin) {}
	function getDateFin() {}
	function setDuree($duree) {}
	function getDuree() {}
}

?>
